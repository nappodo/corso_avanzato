from functools import wraps
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


def logged(func):
    @wraps(func)
    def inner_func(*args, **kwargs):
        # codice che logga i parametri
        logging.debug('{} Parametri args: {}'.format(func.__name__, ', '.join([str(a) for a in args])))
        if kwargs:
            logging.debug('{} Parametri kwargs: {}'.format(func.__name__,
                                                           ','.join(['{}={}'.format(k, v)
                                                                     for k, v in kwargs.items()])))
        res = func(*args, **kwargs)
        # codice che logga il risultato
        logging.debug('{} ha ritornato {}'.format(func.__name__, str(res)))
        return res
    return inner_func


@logged  # new_somma = logged(somma)
def somma(*args):
    return sum(args)

# new_somma = logged(somma)


@logged
def crea_utente(nome, eta=0, nazione='Italia'):
    return nome, eta, nazione


if __name__ == '__main__':
    somma(2, 78, 12, 45)
    crea_utente('John', 48, nazione='UK')
    print(somma.__name__)
