from .con_argomenti import debug


# Class decorator
def debugmethods(cls):
    for name, val in vars(cls).items():
        if callable(val):
            setattr(cls, name, debug(prefix=name.title())(val))
    return cls


@debugmethods
class A1:
    def __init__(self, x):
        self.x = x

    def func_a(self, a, b):
        return (a + b) * self.x

    def func_b(self, a, b, c):
        return (a + b + c) * self.x
