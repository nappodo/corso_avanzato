from functools import wraps
import logging
DEBUG = True  # 'DEBUG' in os.environ

# https://docs.python.org/3/howto/logging.html

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


def debug(prefix=''):
    def decorate(func):
        if not DEBUG:
            return func
        msg = prefix + ' ' + func.__qualname__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log = '{} {} {} {}'.format(msg, 'called with', str(args[1:]), str(kwargs))
            logging.debug(log)
            return func(*args, **kwargs)
        return wrapper
    return decorate


class A:
    @debug(prefix='Class A')
    def func_a(self, a, b):
        return a + b

    @debug(prefix='Class A prefix 2')
    def func_b(self, a, b, c):
        return a + b + c

if __name__ == '__main__':
    obj = A()
    obj.func_a(5, 6)
    obj.func_b(10, 0, 0)
