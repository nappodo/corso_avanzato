import functools
import logging

# https://docs.python.org/3/howto/logging.html
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
DEBUG = True  # os.getenv('DEBUG')


class DebugDecorator:

    def __init__(self, prefix, suffix):
        self.prefix = prefix
        self.suffix = suffix

    def __call__(self, func):
        if not DEBUG:
            return func

        @functools.wraps(func)
        def decorated(*args, **kwargs):
            msg = self.prefix + ' ' + func.__qualname__
            log = '{} {} {} {}'.format(msg, 'called with', str(args[1:]), str(kwargs))
            logging.debug(log)
            return func(*args, **kwargs)
        return decorated


class A:
    @DebugDecorator(prefix='Class A')
    def func_a(self, a, b):
        return a + b

    @DebugDecorator(prefix='Another prefix')
    def func_b(self, a, b, c):
        return a + b + c

if __name__ == '__main__':
    obj = A()
    obj.func_a(5, 6)
    obj.func_b(10, 0, 0)
