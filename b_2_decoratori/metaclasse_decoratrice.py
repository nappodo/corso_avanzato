from .decoratore_classe import debugmethods

# ## metaclasse Decoratrice
DEBUG = True


class DebugMeta(type):
    def __new__(mcs, name, bases, clsdict):
        clsobj = super().__new__(mcs, name, bases, clsdict)
        if DEBUG:
            clsobj = debugmethods(clsobj)
        return clsobj


class ABase(metaclass=DebugMeta):
    pass


class A2(ABase):

    def func_a2(self, a, b):
        return a + b

    def func_b2(self, a, b, c):
        return a + b + c


class A3(ABase):

    def func_a3(self, a, b):
        return a + b

    def func_b3(self, a, b, c):
        return a + b + c