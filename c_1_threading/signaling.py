import logging
import threading

import time

logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s')


def wait_for_event(e):
    logging.debug('waiting for event to be set')
    is_set = e.wait()
    logging.debug('event was set: {}'.format(is_set))
    logging.debug('Processing event')


def wait_for_event_timeout(e, t):
    while not e.is_set():
        logging.debug('waiting for event to be set')
        is_set = e.wait(t)  # è come se fosse una sleep ma termina prima se l'evento è settato
        logging.debug('event was set? {}'.format(is_set))
        if is_set:
            logging.debug('Processing event')
        else:
            logging.debug('Timeout: skip')

if __name__ == '__main__':
    event = threading.Event()
    t1 = threading.Thread(name='block', target=wait_for_event, args=(event,))
    t1.start()
    t2 = threading.Thread(name='nonblock',
                          target=wait_for_event_timeout,
                          args=(event, 3))
    t2.start()
    logging.debug('Waiting before to call event.set()')
    time.sleep(5)
    event.set()
    logging.debug('Event was set')
