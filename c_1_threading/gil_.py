import timeit

t = timeit.Timer('countdown_serial()', 'from c_1_threading.utils import countdown_serial')
print('Serial:', t.timeit(1))
t1 = timeit.Timer('countdown_2_threads()', 'from c_1_threading.utils import countdown_2_threads')
print('Multithreading:', t1.timeit(1))


