import logging
import threading
from random import randint
from time import sleep

logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s')


def worker_sleep(name):
    rand_int_var = randint(1, 5)
    sleep(rand_int_var)
    logging.debug("Thread " + name + " slept for " + str(rand_int_var) + " seconds")


if __name__ == '__main__':
    threads = []
    for i in range(5):
        t = threading.Thread(target=worker_sleep,
                             args=(str(i),),
                             name='Thread ' + str(i))
        threads.append(t)
        t.start()
        # t.join()

    for t in threads:
        t.join()
