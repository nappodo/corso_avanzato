import timeit
import logging

from c_1_threading.utils import calculate_multithreads, calculate_serial

logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s')


if __name__ == "__main__":
    print('------------------- >Number processing')
    import_utils = 'from c_1_threading.utils import calculate_multithreads, calculate_serial'
    numbers = list(range(0, 1000))
    squaredNumbers = calculate_multithreads(numbers, 6)
    squaredNumbers_serial = calculate_serial(numbers)
    t = timeit.Timer('calculate_multithreads(list(range(0, 1000)), 4)',
                     import_utils)
    print('Multithreading:', t.timeit(1))
    t1 = timeit.Timer('calculate_serial(list(range(0, 1000)))',
                      import_utils)
    print('Serial:', t1.timeit(1))


    input('------------------- >File processing')

    t = timeit.Timer('process_lines_parallel(lines, 4)',
                     'from c_1_threading.utils import process_lines_parallel\n'
                     'with open("../data/bigtext.txt") as f:\n'
                     '    lines = f.readlines()')
    print('Multithreading:', t.timeit(1))
    t1 = timeit.Timer('process_lines_serial(lines)',
                      'from c_1_threading.utils import process_lines_serial\n'
                      'with open("../data/bigtext.txt") as f:\n'
                      '    lines = f.readlines()')
    print('Serial:', t1.timeit(1))

    input('------------------- >Only I/O')

    t = timeit.Timer('only_io_parallel(files, 4)',
                     'from c_1_threading.utils import only_io_parallel\n'
                     'files = ["../data/text.txt","../data/bigtext.txt",'
                     '"../data/text.txt","../data/bigtext.txt","../data/text.txt","../data/bigtext.txt"]')
    print('Multithreading:', t.timeit(1))
    t1 = timeit.Timer('only_io_serial(files)',
                      'from c_1_threading.utils import only_io_serial\n'
                      'files = ["../data/text.txt","../data/bigtext.txt",'
                      '"../data/text.txt","../data/bigtext.txt","../data/text.txt","../data/bigtext.txt"]')
    print('Serial:', t1.timeit(1))
