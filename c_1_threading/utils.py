import logging
import threading
from multiprocessing.pool import ThreadPool, Pool
from random import randint
from threading import Thread
from time import sleep

import math

logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s')

COUNT = 100000000


def worker_sleep(name):
    rand_int_var = randint(1, 5)
    sleep(rand_int_var)
    logging.debug("Thread " + name + " slept for " + str(rand_int_var) + " seconds")


def worker(name):
    print(threading.current_thread().name)
    print(threading.active_count())
    logging.debug('Worker: ' + name)


def worker_compute(l):
    for i, elem in enumerate(l):
        l[i] = elem ** 2


def countdown(n):
    while n > 0:
        n -= 1


def countdown_serial():
    countdown(COUNT)


def countdown_2_threads():
    t1 = Thread(target=countdown, args=(COUNT//2,))
    t2 = Thread(target=countdown, args=(COUNT//2,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()


def square_number(n):
    # logging.debug('calculating for ' + str(n))
    return n ** 2


def calculate_multithreads(l, num_threads=2):
    pool = ThreadPool(num_threads)
    results = pool.map(square_number, l)
    pool.close()
    pool.join()
    return results


def calculate_serial(l):
    return [square_number(i) for i in l]


def calculate_multiprocess(l, num_procs=2):
    pool = Pool(num_procs)
    results = pool.map(square_number, l)
    pool.close()
    pool.join()
    return results


def process_line(line):
    # dummy long CPU operation
    _ = [i ** 2 / 0.03 - math.cos(math.log(randint(10, 100))) for i in range(10)]
    return line[::-1]


def process_lines_parallel(lines, num_threads=2):
    pool = ThreadPool(num_threads)
    results = pool.map(process_line, lines)
    pool.close()
    pool.join()
    return results


def process_lines_multiprocess(lines, num_procs=2):
    pool = Pool(num_procs)
    results = pool.map(process_line, lines)
    pool.close()
    pool.join()
    return results


def process_lines_serial(lines):
    return [process_line(line) for line in lines]


def process_lines_multiprocess_2(lines, num_procs=2):
    pool = Pool(num_procs)
    chunk_size = len(lines) // num_procs
    chunks = [lines[i:i + chunk_size] for i in range(0, len(lines), chunk_size)]
    results = pool.map(process_lines_serial, chunks)
    pool.close()
    pool.join()
    return results


def _open_close(filename):
    f = open(filename)
    f2 = open(filename + '_copy', 'wt')
    for line in f:
        f2.write(line)
    f.close()
    f2.close()


def only_io_serial(files):
    for f in files:
        _open_close(f)


def only_io_parallel(files, num_procs=2):
    pool = ThreadPool(num_procs)
    results = pool.map(_open_close, files)
    pool.close()
    pool.join()
    return results
