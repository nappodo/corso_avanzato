import logging
import random
import threading

import time


logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s')


class Counter:
    """
    Classe thread safe
    """
    def __init__(self, start=0):
        self.lock = threading.Lock()
        self.value = start
        self.serialized_consumers = []

    def inc(self):
        logging.debug('Waiting for lock')
        self.lock.acquire()  # commentare per vedere che succede
        try:
            logging.debug('Acquired lock')
            self.serialized_consumers.append(threading.current_thread().name)
            self.value += 1  # con i lock si serializza l'accesso ad una variabile.
            logging.debug('{}{}'.format(self.value, ''))
        finally:
            # pass
            self.lock.release()


def worker(counter):
    for _ in range(1):
        sleep_value = random.randint(1, 3)
        logging.debug('Sleeping for {} secs'.format(sleep_value))
        time.sleep(sleep_value)  # il thread in esecuzione viene sospeso
        counter.inc()
    logging.debug('Done')


if __name__ == '__main__':
    c = Counter()
    threads = []
    for i in range(5):  # for i in range(100000):
        t = threading.Thread(target=worker, args=(c, ))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()
    logging.debug('Counter value {} {}'.format(c.value, c.serialized_consumers))
    # logging.debug('Counter value {} {}'.format(c.value, ''))
