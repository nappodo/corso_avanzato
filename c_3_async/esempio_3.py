import asyncio
import signal

import aiohttp
import aiohttp.web
import sys
import json


WEBSITES = ['http://example.com/',
            'http://dummy-a98x3.org',
            'http://example.net/']


async def handle(request):
    # le richieste aiohttp sono asincrone

    coroutines = [session.get(website) for website in WEBSITES]

    # Le coroutine sono lanciate in parallelo
    results = await asyncio.gather(*coroutines, return_exceptions=True)  # gather di tutti i future

    # Analyze results
    response_data = {
        website: not isinstance(result, Exception) and result.status == 200
        for website, result in zip(WEBSITES, results)
    }
    for result in results:
        if not isinstance(result, Exception):
            await result.wait_for_close()

    # Build JSON response
    body = json.dumps(response_data).encode('utf-8')
    return aiohttp.web.Response(body=body, content_type="application/json")


def signal_handler(signal, frame):  # intercetta CTRL+C tramite signal.SIGINT
    loop.stop()
    server.close()
    session.close()
    app.cleanup()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
session = aiohttp.ClientSession()
loop = asyncio.get_event_loop()
app = aiohttp.web.Application(loop=loop)
app.router.add_route('GET', '/', handle)
server = loop.create_server(app.make_handler(), '127.0.0.1', 8000)
print("Server started at http://127.0.0.1:8000")
loop.run_until_complete(server)
loop.run_forever()
