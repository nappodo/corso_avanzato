import asyncio
import functools
import requests
import time

ts = time.time()


@asyncio.coroutine
def do_checks():
    for i in range(10):
        loop = asyncio.get_event_loop()
        req = loop.run_in_executor(None, functools.partial(requests.get, "http://google.com", timeout=3))
        resp = yield from req
        print(resp.status_code)

loop = asyncio.get_event_loop()
loop.run_until_complete(do_checks())
te = time.time()
print("Version A: " + str(te - ts))

ts = time.time()
for i in range(10):
    r = requests.get("http://google.com", timeout=3)
    print(r.status_code)
te = time.time()

print("Version B:  " + str(te - ts))
