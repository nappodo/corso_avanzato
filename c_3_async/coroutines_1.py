def my_coroutine():
    while True:
        received = yield
        print('Received:', received)


def minimize():
    current = yield
    while True:
        value = yield current
        current = min(value, current)

# it = my_coroutine()
# next(it)             # Inizia ad eseguire la coroutine. Arriva fino al primo yield e aspetta il send.
# it.send('First')
# it.send('Second')
input('\n\nSecondo esempio\n')
it = minimize()
next(it)
print(it.send(10))
print(it.send(4))
print(it.send(25))
print(it.send(22))
print(it.send(-1))

input('Coroutine asincrona')
