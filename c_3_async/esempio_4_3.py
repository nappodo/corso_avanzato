import aiohttp
import asyncio
import time

loop = asyncio.get_event_loop()
ts = time.time()

async def main(n):
    session = aiohttp.ClientSession()
    fs = [session.get('http://google.com') for _ in range(n)]
    for f in asyncio.as_completed(fs):
        resp = await f
        print(resp.status)
        await resp.release()
    session.close()


loop.run_until_complete(main(10))
loop.close()
te = time.time()
print("Aiohttp version:  " + str(te - ts))
