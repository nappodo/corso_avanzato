import asyncio
import aiohttp
import random


BASE_URL = 'http://quotes.toscrape.com/page/{page_num}'


def generate_urls(num):
    for i in range(num):
        page_num = random.randint(1, 50)
        yield BASE_URL.format(page_num=page_num)


def chunked_http_client(http_client, num_chunks):
    semaphore = asyncio.Semaphore(num_chunks)  # per limitare il numero di client

    @asyncio.coroutine  # stile Python < 3.5
    def http_get(url):
        nonlocal semaphore
        with (yield from semaphore):
            res = yield from http_client.get(url)
            body = yield from res.content.read()
            yield from res.wait_for_close()
        return body
    return http_get  # ritorna una coroutine che rispetta il semaforo ed effettua richieste HTTP asincrone


def run(http_client, num):
    urls = generate_urls(num)  # è un generatore
    http_client = chunked_http_client(http_client, 100)  # è una corooutine
    tasks = [http_client(url) for url in urls]  # è una lista di futures/tasks
    responses_sum = 0
    for future in asyncio.as_completed(tasks):
        data = yield from future
        responses_sum += len(data)
    return responses_sum


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    with aiohttp.ClientSession(loop=loop) as client:
        # client = aiohttp.ClientSession(loop=loop)
        num_iter = 500
        result = loop.run_until_complete(run(client, 100))
        client.close()
        print(result)
