import asyncio
import random


async def slow_operation(n):
    pause = random.randint(1, 5)
    await asyncio.sleep(pause)
    print("Slow operation {} complete (paused {})".format(n, pause))


async def main():
    await asyncio.wait([
        slow_operation(1),
        slow_operation(2),
        slow_operation(3),
    ])


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
