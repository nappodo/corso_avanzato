import signal
import sys
import asyncio
import aiohttp
import json


async def get_json(client_, url):
    async with client_.get(url) as response:
        # assert response.status == 200
        return await response.read()


async def get_reddit_top(subreddit, client_):
    # await = yield from
    data1 = await get_json(client_, 'https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=day&limit=5')

    j = json.loads(data1.decode('utf-8'))
    for i in j['data']['children']:
        print('{score}: {title} ({url})'.format(**i['data']))

    print('DONE:', subreddit + '\n')


loop = asyncio.get_event_loop()
client = aiohttp.ClientSession(loop=loop)


def signal_handler(signal, frame):  # intercetta CTRL+C tramite signal.SIGINT
    loop.stop()
    client.close()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)  # intercetta CTRL+C tramite signal.SIGINT

asyncio.ensure_future(get_reddit_top('python', client))  # "wrappa" una coroutine in un Future
asyncio.ensure_future(get_reddit_top('programming', client))
asyncio.ensure_future(get_reddit_top('compsci', client))
loop.run_forever()
