import json
from multiprocessing.pool import ThreadPool

import requests


def get_json(url):
    response = requests.get(url)
    return response.content


def get_reddit_top(subreddit):
    data1 = get_json('https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=day&limit=5')
    j = json.loads(data1.decode('utf-8'))
    res = ''
    for i in j['data']['children']:
        res += '\n{score}: {title} ({url})'.format(**i['data'])
    return res

if __name__ == '__main__':

    pool = ThreadPool(3)
    results = pool.map(get_reddit_top, ('python', 'programming', 'compsci'))
    pool.close()
    pool.join()
    for r in results:
        print(r)
