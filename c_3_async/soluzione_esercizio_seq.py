import json
import requests


def get_json(url):
    response = requests.get(url)
    return response.content


def get_reddit_top(subreddit):
    data1 = get_json('https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=day&limit=5')

    j = json.loads(data1.decode('utf-8'))
    for i in j['data']['children']:
        print('{score}: {title} ({url})'.format(**i['data']))

    print('DONE:', subreddit + '\n')


get_reddit_top('python')  # "wrappa" una coroutine in un Future
get_reddit_top('programming')
get_reddit_top('compsci')
