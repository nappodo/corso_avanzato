from types import coroutine


@coroutine
def coro_1():
    print('coro 1')
    a = yield 'text'
    print('Result ', a)
    return a


@coroutine
def coro_2():
    print('coro 2')
    a = yield from coro_1()
    print('Result 2', a)


if __name__ == '__main__':
    f = coro_2()
    # print(f)
    f.send(None)
    f.send(42)