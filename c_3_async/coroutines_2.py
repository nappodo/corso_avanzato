import asyncio

USE = 'tasks'

async def some_remote_call(a):
    await asyncio.sleep(int(a))
    return str(a)

async def foo(arg):
    result = await some_remote_call(arg)
    return result.upper()


if __name__ == '__main__':
    if USE == 't':
        # Tasks version
        coros = []
        for i in range(5):
            coros.append(foo(i))

        loop = asyncio.get_event_loop()
        futures = asyncio.wait(coros)
        loop.run_until_complete(futures)
    else:
        #  Future version
        futures = []
        for i in range(5):
            futures.append(asyncio.ensure_future(foo(i)))

        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait(futures))
    print([f.result() for f in futures])

