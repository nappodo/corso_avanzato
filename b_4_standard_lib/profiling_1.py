import timeit


t1 = timeit.Timer('f1(50, 50, 10000)', 'from b_4_standard_lib.utils import f1')
t2 = timeit.Timer('f2(50, 50, 10000)', 'from b_4_standard_lib.utils import f2')
t3 = timeit.Timer('f3(50, 50, 10000)', 'from b_4_standard_lib.utils import f3')
# esegue f 1000 volte e ritorna il tempo cumulato
print(t1.timeit(100))
print(t2.timeit(100))
print(t3.timeit(100))
# esegue t.timeit(1000) 10 volte e ritorna i risultati di timeit in una lista di 10
input('repeat...')
print(t1.repeat(5, 100))
print(t2.repeat(5, 100))
print(t3.repeat(5, 100))
