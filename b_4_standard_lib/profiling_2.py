import cProfile as profile

from b_4_standard_lib.utils import fibonacci, fibonacci_memo


# profile.run('print(fibonacci(32))')
profile.run('print(fibonacci_memo(32))')
