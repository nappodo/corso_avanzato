import math
import os
import sys
from contextlib import contextmanager


@contextmanager
def silence_stdout():
    new_target = open(os.devnull, "w")
    old_target, sys.stdout = sys.stdout, new_target
    try:
        yield new_target
    finally:
        sys.stdout = old_target


def f1(x, y, n):
    l = []
    for i in range(n):
        l.append((i, 'Risultato: {}'.format(math.sqrt(x + y) * math.sqrt(x * i))))
    with silence_stdout():
        for i, res in l:
            if i == 50:
                break
            print(i, res, sep=') ')


def f2(x, y, n):
    s = {}
    for i in range(n):
        s[i] = 'Risultato: {}'.format(math.sqrt(x + y) * math.sqrt(x * i))
    with silence_stdout():
        for i, res in s.items():
            if i == 50:
                break
            print(i, res, sep=') ')


def f3(x, y, n):
    gen = ((i, 'Risultato: {}'.format(math.sqrt(x + y) * math.sqrt(x * i))) for i in range(n))
    with silence_stdout():
        for i, res in gen:
            if i == 50:
                break
            print(i, res, sep=') ')


def fibonacci_raw(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci_raw(n - 1) + fibonacci_raw(n - 2)


def fibonacci(n):
    l = []
    if n > 0:
        l.extend(fibonacci(n - 1))
    l.append(fibonacci_raw(n))
    return l


class Memoize:
    def __init__(self, func):
        self.f = func
        self.memo = {}

    def __call__(self, *args, **kwargs):
        if args not in self.memo:
            self.memo[args] = self.f(*args)
        return self.memo[args]


@Memoize
def fibonacci_raw_memo(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci_raw_memo(n - 1) + fibonacci_raw_memo(n - 2)


def fibonacci_memo(n):
    l = []
    if n > 0:
        l.extend(fibonacci_memo(n - 1))
    l.append(fibonacci_raw_memo(n))
    return l
