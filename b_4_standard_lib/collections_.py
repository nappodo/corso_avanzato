from collections import defaultdict, deque


def test_defaultdict():
    d = {}
    for i in range(500):
        if i % 3 == 0:
            if 3 in d:
                d[3].append(i)
            else:
                d[3] = [i]
        elif i % 5 == 0:
            if 5 in d:
                d[5].append(i)
            else:
                d[5] = [i]

    [print(k, v, sep=': ') for k, v in d.items()]

    d2 = {}
    for i in range(500):
        if i % 3 == 0:
            d2[3] = [i] if 3 not in d2 else d2[3] + [i]
        elif i % 5 == 0:
            d2[5] = [i] if 5 not in d2 else d2[5] + [i]

    [print(k, v, sep=': ') for k, v in d2.items()]

    ###############################
    d3 = defaultdict(list)
    for i in range(500):
        if i % 3 == 0:
            d3[3].append(i)
        elif i % 5 == 0:
            d3[5].append(i)

    [print(k, v, sep=': ') for k, v in d3.items()]

    ###############################
    class MyObject:
        x = 1000

        def __repr__(self):
            return repr(r'My Object instance... ' + repr(self.x))

    d1 = defaultdict(MyObject)
    print(d1['new'])

    ###############################
    input('counters')
    counters = defaultdict(int)
    for i in range(500):
        if i % 3 == 0:
            counters[3] += 1
        elif i % 5 == 0:
            counters[5] += 1
    print(counters)


# deque (coda a doppia entrata/uscita)
def tail(filename, n=10):
    """Return the last n lines of a file"""
    with open(filename) as f:
        return deque(f, n)


def moving_average(iterable, n=3):
    # moving_average([40, 30, 50, 46, 39, 44]) --> 40.0 42.0 45.0 43.0
    # http://en.wikipedia.org/wiki/Moving_average
    d = deque(iterable[:n-1], n)
    d.appendleft(0)  # deque([0, 40, 30])
    s = sum(d)
    for elem in iterable[n-1:]:  # 50, 46, 39, 44
        s += elem - d.popleft()
        d.append(elem)
        yield s / n


input('Default dict')
test_defaultdict()
input('\n\ntail......')
t = tail('../data/text.txt')
for line in t:
    print(line)

input('\n\nMoving average\n\n')
print(list(moving_average([40, 30, 50, 46, 39, 44])))
