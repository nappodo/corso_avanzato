import os
import glob

script_dir = os.path.dirname(__file__)
print(script_dir)
print(os.path.isfile(__file__))
print(os.path.exists(__file__))
print(os.pathsep)
print(os.defpath)
print(os.getenv('PATH'))
print(os.getlogin())
print(os.getcwd())

path_conversion = os.path.normpath('/home/dominik/Desktop/avanzato/../avanzato')  # converte anche il separatore
print(path_conversion)
input()
try:
    import example
except ImportError as e:
    print('Errore: --->', e)


new_module_dir = os.path.join('/home/dominik/Desktop/avanzato/', 'python_tests')
import sys
before_len = len(sys.path)
import site
site.addsitedir(new_module_dir)
print(sys.path[before_len:])

print(glob.glob(sys.path[before_len:][0] + '/*.py'))
import example

