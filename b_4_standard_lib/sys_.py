import sys

for attr in ('version', 'version_info', 'platform',
             'path', 'modules', 'executable', 'prefix', 'argv'):
    print(attr, getattr(sys, attr), sep='---->')

input()
sys.stdout.write('Std OUT: data\n\n')
sys.stdout.flush()
print(type(sys.stdout), sys.stdout)
print(type(sys.stdin), sys.stdin)

input('ref counts')


a = [10, 110, 100000]
# c'è un reference temporaneo in più ad obj
# quando chiamiamo getrefcount(obj)
print(sys.getrefcount(a))
b = a
print(sys.getrefcount(a))
c = b
print(sys.getrefcount(a))
input('size of an object')
print(sys.getsizeof(a))  # __size_of__ special method per le classi definite da utente

sys.exit(1)


