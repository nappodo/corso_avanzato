import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
db_file = os.path.normpath(os.path.join(os.path.dirname(__file__), '../../data/orm_test.db'))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////{}'.format(db_file)
db = SQLAlchemy(app)


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    surname = db.Column(db.String(80))
    telephone = db.Column(db.String(120))
    address = db.Column(db.Text(1200))
    email = db.Column(db.String(120))

    def __init__(self, name, surname, telephone, address, email):
        self.name = name
        self.surname = surname
        self.telephone = telephone
        self.address = address
        self.email = email

    def __repr__(self):
        return '<Contact %r>' % self.id


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text(1000))
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))
    contact = db.relationship('Contact', backref=db.backref('note', lazy='dynamic'))

    def __init__(self, text, contact):
        self.text = text
        self.contact = contact

    def __repr__(self):
        return '<Note %r>' % self.id

