from d_2_web.rubrica_orm.main import db, app


def main():
    db.create_all()
    app.run(debug=True)


if __name__ == '__main__':
    # db.drop_all()
    db.create_all()
    app.run(debug=True)

