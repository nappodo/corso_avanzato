from flask import render_template, request, url_for, redirect
from .models import app, db, Contact, Note


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/lista_contatti')
def lista_contatti():
    contacts = Contact.query.all()
    return render_template('list.html', results=contacts)


@app.route('/rimuovi/<int:id_>')
def rimuovi(id_):
    contact = Contact.query.filter_by(id=id_).first()
    db.session.delete(contact)
    db.session.commit()
    return redirect(url_for('lista_contatti'))


@app.route('/aggiungi_nota', methods=['GET', 'POST'])
def aggiungi_nota():
    if request.method == 'POST':
        contact_id = request.form.get('contact_id')
        contact = Contact.query.filter_by(id=contact_id).first()
        note = Note(request.form.get('text_'), contact)
        db.session.add(note)
        db.session.commit()
        return redirect(url_for('dettaglio', id_=contact_id))


@app.route('/delete_note/<int:id_>/<int:contact_id>')
def delete_note(id_, contact_id):
    note = Note.query.filter_by(id=id_).first()
    db.session.delete(note)
    db.session.commit()
    return redirect(url_for('dettaglio', id_=contact_id))


@app.route('/dettaglio/<int:id_>')
def dettaglio(id_):
    contact = Contact.query.filter_by(id=id_).first()
    notes = Note.query.filter_by(contact_id=id_)
    contact.notes = notes
    return render_template('aggiungi.html', result=contact)


@app.route('/aggiungi_contatto', methods=['GET', 'POST'])
def aggiungi():

    if request.method == 'POST':
        nome = request.form.get('name_')
        cognome = request.form.get('surname_')
        numero = request.form.get('number_')
        indirizzo = request.form.get('address_')
        email = request.form.get('email_')
        id_ = request.form.get('id_')
        if id_:
            contact = Contact.query.filter_by(id=int(id_)).first()
            contact.name = nome
            contact.surname = cognome
            contact.telephone = numero
            contact.address = indirizzo
            contact.email = email
        else:
            # new contact
            contact = Contact(nome, cognome, numero, indirizzo, email)
            db.session.add(contact)
        db.session.commit()
        return redirect(url_for('lista_contatti'))

    else:
        return render_template('aggiungi.html', result={})