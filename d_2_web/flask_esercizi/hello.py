from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Hello World!'


@app.route('/list_contatti')
def lista():
    return 'Lista contatti 222!'


@app.route('/hello/<user>')
def hello_user(user=None):
    return render_template('hello.html', name=user)


if __name__ == '__main__':
    app.run(debug=True)
