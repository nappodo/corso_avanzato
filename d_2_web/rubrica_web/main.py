from flask import Flask, render_template, request, url_for, redirect
import sqlite3

from d_2_web.rubrica_web.utils import dict_factory

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/lista_contatti')
def lista_contatti():
    stmt = 'SELECT id_, name_, surname_, number_ FROM rubrica'
    cursor = cnx.cursor()
    cursor.execute(stmt)
    results = []
    app.logger.debug(stmt)
    for row in cursor:
        app.logger.debug(row)
        results.append(dict_factory(cursor, row))
    cursor.close()
    return render_template('list.html', results=results)


@app.route('/rimuovi/<int:id_>')
def rimuovi(id_):
    stmt = "DELETE FROM rubrica WHERE id_=?"
    cursor = cnx.cursor()
    cursor.execute(stmt, (id_,))
    cursor.close()
    cnx.commit()
    return redirect(url_for('lista_contatti'))


@app.route('/aggiungi_nota', methods=['GET', 'POST'])
def aggiungi_nota():
    if request.method == 'POST':
        contact_id = request.form.get('contact_id')
        text = request.form.get('text_')
        stmt = 'INSERT INTO notes(text_, contact_id) VALUES(?, ?)'
        cursor = cnx.cursor()
        cursor.execute(stmt, (text, contact_id))
        cursor.close()
        cnx.commit()
        return redirect(url_for('dettaglio', id_=contact_id))


@app.route('/delete_note/<int:id_>/<int:contact_id>')
def delete_note(id_, contact_id):
    stmt = 'DELETE FROM notes WHERE id_=?'
    cursor = cnx.cursor()
    cursor.execute(stmt, (id_,))
    cursor.close()
    cnx.commit()
    return redirect(url_for('dettaglio', id_=contact_id))


@app.route('/dettaglio/<int:id_>')
def dettaglio(id_):
    stmt = """SELECT id_, name_, surname_, number_, email_, address_ FROM rubrica WHERE id_=?"""
    cursor = cnx.cursor()
    cursor.execute(stmt, (id_,))
    res = cursor.fetchone()
    res = dict_factory(cursor, res)
    res['notes'] = []

    stmt = """SELECT id_, text_ FROM notes WHERE contact_id=?"""

    cursor.execute(stmt, (id_,))
    for note in cursor:
        note = dict_factory(cursor, note)
        res['notes'].append((note['id_'], note['text_']))

    cursor.close()
    return render_template('aggiungi.html', result=res)


@app.route('/aggiungi_contatto', methods=['GET', 'POST'])
def aggiungi():

    if request.method == 'POST':
        nome = request.form.get('name_')
        cognome = request.form.get('surname_')
        numero = request.form.get('number_')
        indirizzo = request.form.get('address_')
        email = request.form.get('email_')
        id_ = request.form.get('id_')
        if id_:
            stmt = 'UPDATE rubrica SET name_=?, surname_=?, number_=?, address_=?, email_=? WHERE id_=?'

            tupla_args = (nome, cognome, numero, indirizzo, email, id_)
        else:
            stmt = 'INSERT INTO rubrica(name_, surname_, number_, address_, email_) VALUES(?, ?, ?, ?, ?)'
            tupla_args = (nome, cognome, numero, indirizzo, email)

        cur = cnx.cursor()
        cur.execute(stmt, tupla_args)
        cur.close()
        cnx.commit()
        return redirect(url_for('lista_contatti'))

    else:
        return render_template('aggiungi.html', result={})


def init_db(cnx):
    # drop_table = """DROP TABLE rubrica"""
    sql_create_contatti = """CREATE TABLE IF NOT EXISTS rubrica(
      id_ INTEGER PRIMARY KEY AUTOINCREMENT,
      name_ text,
      surname_ text,
      number_ text,
      address_ text,
      email_ text
    )
    """

    cursor_ = cnx.cursor()
    # cursor_.execute(drop_table)
    cursor_.execute(sql_create_contatti)
    cnx.commit()
    # drop_table = """DROP TABLE notes"""
    # cursor_.execute(drop_table)
    sql_create_notes = """CREATE TABLE IF NOT EXISTS notes(
      id_ INTEGER PRIMARY KEY AUTOINCREMENT,
      text_ text,
      contact_id int REFERENCES rubrica(id_)
    )
    """
    cursor_.execute(sql_create_notes)
    cnx.commit()
    cursor_.close()


if __name__ == '__main__':
    dbfile = '../../data/example.db'
    # sqlite3.connect(':memory:') per database in memoria
    cnx = sqlite3.connect(dbfile, check_same_thread=False)
    init_db(cnx)
    app.run(debug=True)
