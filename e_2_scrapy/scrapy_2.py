from urllib.parse import urlparse

import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.crawler import CrawlerProcess
from scrapy.http import Request
from scrapy.item import Item, Field
from scrapy.utils.project import get_project_settings


class QuoteItem(Item):
    text = Field()
    link = Field()
    author = Field()
    image_urls = Field()
    images = Field()


class MyImagePipeline(ImagesPipeline):
    def file_path(self, request, response=None, info=None):
        if not isinstance(request, Request):
            url = request
        else:
            url = request.url
        parsed_url = urlparse(url)
        domain = parsed_url.netloc.replace('www.', '').replace('.', '_')
        img_path = parsed_url.path.replace('/', '_')
        return 'full/{}_{}'.format(domain, img_path)

    def thumb_path(self, request, thumb_id, response=None, info=None):
        file_path = self.file_path(request)
        return 'thumbs/{}/{}'.format(thumb_id, file_path).replace('full/', '')


class WriterPipeline:
    def __init__(self):
        self.file = open('quotes2.txt', 'w')

    @staticmethod
    def quote_to_line(q):
        return '{} ({}) - {}\n'.format(q['text'], q['author'], q['link'])

    def process_item(self, item, spider):
        line = self.quote_to_line(item)
        self.file.write(line)
        return item


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'http://www.brainyquote.com/quotes/topics/topic_life.html',
    ]

    @classmethod
    def generate_urls(cls):
        for i in range(2, 10):
            yield 'http://www.brainyquote.com/quotes/topics/topic_life{}.html'.format(i)

    def parse(self, response):
        for quote in response.css('.masonryitem'):
            qi = QuoteItem(text=quote.css('a[title="view quote"]::text').extract_first(),
                           link=quote.css('a[title="view quote"]').xpath('@href').extract_first(),
                           author=quote.css('a[title="view author"]::text').extract_first(),
                           image_urls=[response.urljoin(quote.css('img[class=" zoomc bqpht"]').xpath('@src').extract_first())])
            yield qi

        try:
            next_page = next(generate_urls_generator)
            if next_page is not None:
                # next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse)
        except StopIteration:
            pass


generate_urls_generator = QuotesSpider.generate_urls()

if __name__ == '__main__':
    settings = get_project_settings()
    settings.update({'IMAGES_STORE': '/home/dominik/altri_clienti/synergia/corso_settembre_2016/avanzato/corso_avanzato/e_2_scrapy/images',
                     'ITEM_PIPELINES': {'e_2_scrapy.scrapy_2.WriterPipeline': 100,
                                        'e_2_scrapy.scrapy_2.MyImagePipeline': 1},
                                        # 'scrapy.pipelines.images.ImagesPipeline': 1},
                    'IMAGES_THUMBS': {'small': (50, 50), 'big': (270, 270)}})
    process = CrawlerProcess(settings)
    process.crawl(QuotesSpider)
    process.start()  # the script will block here until the crawling is finished
