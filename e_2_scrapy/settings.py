# Obey robots.txt rules
ROBOTSTXT_OBEY = True
BOT_NAME = 'tutorial'

SPIDER_MODULES = ['e_2_scrapy.scrapy_2']
ITEM_PIPELINES = {
    'e_2_scrapy.scrapy_2.WriterPipeline': 100,
}