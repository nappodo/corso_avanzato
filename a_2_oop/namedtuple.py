from collections import namedtuple

Auto = namedtuple('Auto', 'marca, modello, cilindrata, anno')

focus = Auto(marca='Ford', modello='Focus', cilindrata=1600, anno=2005)
print(focus)
print(focus.marca)
print(focus.modello)
print(focus[3])

# Tuple come dizionario
d = Auto._asdict(focus)
print(d)

focus_2 = Auto(**d)
print(focus_2)


