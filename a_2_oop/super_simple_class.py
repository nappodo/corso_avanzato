#!/usr/bin/env python
# -*- coding: UTF-8 -*-

x = 2
print(type(x))
print(type(type(x)))


input('Classe senza attributi')


class A:
    pass

a_object = A()
a_object.x = 2
print(a_object.x)
print(type(a_object))
print(type(A))


class A1:

    def __init__(self, a, b=0):
        self.a = a
        self.b = b

    def get_sum(self):
        return self.a + self.b


input('Classe con init')
a1 = A1(5)
print(a1.get_sum())
# print(A1.get_sum(a1))