import abc
import json

DECODER_REGISTER = {}


# metaclasse per gestire registro di classi
# deriva da ABCMeta, quindi chi la usa come metaclasse crea una classe astratta
class EncoderMeta(abc.ABCMeta):
    def __new__(mcs, name, bases, clsdict):
        clsobj = super().__new__(mcs, name, bases, clsdict)
        if 'Base' not in name:
            DECODER_REGISTER[name] = clsobj
        return clsobj


class EncoderBase(metaclass=EncoderMeta):
    def __init__(self, data):
        self.data = data

    @classmethod
    def name(cls):
        return '{}: {}'.format(str(type(cls)), str(cls.__name__).title())

    @abc.abstractmethod
    def encode(self):
        """Retrieve data from the input source and return an object."""
        return


class SimpleTextEncoder(EncoderBase):
    template = '{}: {}\n'

    def encode(self):
        res = ''
        for k, v in self.data.items():
            res += self.template.format(k.upper(), v)
        return res


class HtmlEncoder(SimpleTextEncoder):
    template = '<h3>{}</h3><p>{}</p><br />\n'


class JsonEncoder(EncoderBase):
    def encode(self):
        return json.dumps(self.data)


class MyEncoder(HtmlEncoder):
    def encode(self):
        res = super().encode()
        res += '<p>Visita il sito di <a href="domeniconappo.org">domenico nappo</a>!!!</p>'
        return res


if __name__ == '__main__':
    data_ = {'title': 'Il Nome della Rosa', 'author': 'Umberto Eco', 'price': 22.5}
    encoded = []
    for encoder_cls in DECODER_REGISTER.values():
        print('Encoding with class: ', encoder_cls.name())
        encoder_obj = encoder_cls(data_)
        encoded.append(encoder_obj.encode())
    print('\n')
    print('\n\n\n'.join(s for s in encoded))
