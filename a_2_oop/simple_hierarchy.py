#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from collections import namedtuple

Suit = namedtuple('Suit', 'name, symbol')


class Card:
    def __init__(self, rank, suit):
        """

        :param rank:
        :param suit: instance of Suit
        """
        self.suit = suit
        self.rank = rank
        self.points = self._points()

    def _points(self):
        raise NotImplementedError()


class NumberCard(Card):
    def _points(self):
        return int(self.rank)


class AceCard(Card):
    def _points(self):
        return 11


class FaceCard(Card):
    def _points(self):
        return 10


# ####### factory method


def build_card(rank, suit):
    if rank == 1:
        return AceCard('A', suit)
    elif 2 <= rank < 11:
        return NumberCard(str(rank), suit)
    elif 11 <= rank < 14:
        name = {11: 'J', 12: 'Q', 13: 'K'}[rank]
        return FaceCard(name, suit)

if __name__ == '__main__':
    cuori_10 = NumberCard(10, Suit('heart', '♥'))
    # print(str(cuori_10.hard))

    suits = {'spade': Suit('Spade', '♠'),
             'club': Suit('Club', '♣'),
             'heart': Suit('Heart', '♥'),
             'diamond': Suit('Diamond', '♦')}
    deck = [build_card(rank, suits[suit]) for rank in range(1, 14) for suit in suits]
    print('\n'.join([str(d) for d in deck]))


