import datetime


def sconto(tuple_obj, tuple_client):
    type_ = tuple_obj[0]
    pub_date = tuple_obj[1]
    price = tuple_obj[2]
    if type_ == 'book' and pub_date < datetime.datetime(2016, 1, 1):
        price -= 30 / 100 * price
    elif type == 'cd':
        price -= 15 / 100 * price
    client_type = tuple_client[0]
    if client_type == 'fidelity':
        price -= 10/100 * price
    return price

if __name__ == '__main__':
    tupla_articolo = ('book', datetime.datetime(2014, 1, 1), 15.40)
    tupla_cliente = ('fidelity', 'Mario', 'Rossi')

    new_price = sconto(tupla_articolo, tupla_cliente)
    print('Il prezzo scontato è {:.2f} €'.format(new_price))

    tupla_cliente = ('normal', 'Mario', 'Rossi')

    new_price = sconto(tupla_articolo, tupla_cliente)
    print('Il prezzo scontato è {:.2f} €'.format(new_price))
