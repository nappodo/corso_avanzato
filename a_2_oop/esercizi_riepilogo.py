"""
Esercizio 1:

Scrivere un programma che, date due coppie di coordinate rappresentanti due punti
dello spazio, ne calcoli la distanza.

Implementare inoltre una property che ritorni la distanza dall'origine.


Utilizzare le classi per implementare la soluzione
ed un main che testi il funzionamento con due punti:

Implementare inoltre i metodi per testare:

p1 == p2 True se p1 e p2 hanno le stesse coordinate x e y
p1 >= p2 True se p1 è equidistante o più lontano dall'origine di p2


















Esercizio 2:
Scrivere una classe contenitore di oggetti (istanze della classe dell'Esercizio 1')
e che implementi il protocollo di iterazione e una proprerty mindistance

ps = PointSet(Point(1,1),Point(1,2))
ps.add(Point(2,2), Point(10,2))
ps.mindistance  # ritorna la distanza minima fra i punti

Point(1,1) in ps  # True
for p in ps:
  print(p)  # deve stampare Point(1, 1)











Esercizio 3:
Scrivere una classe di tipo Mapping (e cioè che derivi
dalla classe/interfaccia collections.UserDict) e che
permetta l'operazione '+' tra dizionari (aggiunta di chiavi,
 non addizioni fra i valori con chiave corrispondente!).
Nota: bisogna implementare tutti i metodi astratti della classe UserDict
      altrimenti non sarà possibile istanziare
Es:

>>>d1 = MyDict(a=3, b=2)
>>>d2 = MyDict(c=100)
>>>d3 = d1 + d2  # __add__
>>>print(d3)
<<<'MyDict(a=3, b=2, c=100)'

NB: d1 e d2 restano inalterati. Viene ritornato un nuovo dizionario
"""

