#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import random


class Suit:
    def __init__(self, name, symbol):
        self.name = name
        self.symbol = symbol

    def __str__(self):
        return self.symbol

    def __hash__(self):
        # in questo modo, si può utilizzare direttamente
        # l'oggetto Suit come chiave di un dizionario
        # invece che ad esempio usare la proprietà 'symbol'
        # Vedere la classe Deck per un esempio di utilizzo.
        return hash(self.symbol)


class Suits:
    # attributi di classe
    suits = {'spade': Suit('Spade', '♠'),
             'club': Suit('Club', '♣'),
             'heart': Suit('Heart', '♥'),
             'diamond': Suit('Diamond', '♦')}
    heart = suits['heart']
    club = suits['club']
    diamond = suits['diamond']
    spade = suits['spade']
    multipliers = {spade: 1,
                   club: 2,
                   heart: 3,
                   diamond: 4}

    @classmethod
    def get(cls, name):
        """
        Ritorna un seme per nome o per simbolo
        :param name: nome o simbolo del seme
        :return: istanza della classe Suit
        """
        suit = cls.suits.get(name.lower())
        if not suit:
            for s in cls.suits.values():
                if s.symbol == name:
                    return s
        return suit

    @staticmethod
    def translate(symbol, lang='it'):
        return {'it': {'spade': 'picche',
                       'club': 'fiori',
                       'heart': 'cuori',
                       'diamond': 'quadri'}
                }[lang][symbol]


# Questa classe dovrebbe essere astratta e non istanziabile
class Card:
    def __init__(self, rank, suit, points):
        """
        :param rank: 2,...,10, J, Q, K, A
        :param suit: '♠', '♣', '♥', '♦'
        :param points: a number
        """
        self.rank = rank
        self.suit = suit
        self.base_points = points
        self._internal_cache = None

    def __str__(self):
        return '{}{}'.format(self.rank, self.suit)

    @property  # getter
    def points(self):
        # una semplice cache interna
        if self._internal_cache is None:
            self._internal_cache = self._points()
        return self._internal_cache

    def _points(self):
        raise NotImplementedError()

    @points.setter
    def points(self, value):
        pass

    @points.deleter
    def points(self):
        pass

    def __eq__(self, other):
        return other.rank == self.rank and other.suit == self.suit

        # per implementare <, >, <=, >= vedere __gt__, __lt__, __ge__, __le__

        # per utilizzare oggetti della classe come chiavi di un dizionario,
        # implementare __hash__


class NumberCard(Card):
    def __init__(self, rank, suit):
        super().__init__(str(rank), suit, rank)

    def _points(self):
        return self.base_points * Suits.multipliers[self.suit]


class AceCard(Card):
    def __init__(self, suit):
        super().__init__('A', suit, 100)

    def _points(self):
        return self.base_points


class FaceCard(Card):
    _facepoints = {'J': 10, 'Q': 20, 'K': 30}

    def __init__(self, rank, suit):
        face = {11: 'J', 12: 'Q', 13: 'K'}[rank]
        super().__init__(face, suit, self._facepoints[face])

    def _points(self):
        return self.base_points * Suits.multipliers[self.suit]


class CardFactory:
    def __call__(self, *args, **kwargs):
        rank = args[0]
        suit = args[1]
        if rank == 1:
            return AceCard(suit)
        elif 2 <= rank < 11:
            return NumberCard(str(rank), suit)
        elif 11 <= rank < 14:
            return FaceCard(rank, suit)


class Deck:

    def __init__(self):
        # gli attributi e i metodi che iniziano con '_' sono da considerarsi privati!!!
        card_factory = CardFactory()
        self._cards = {s: [] for s in Suits.suits.values()}
        for r, s in ((r, s) for r in range(1, 14) for s in Suits.suits.values()):
            self._cards[s].append(card_factory(r, s))  # creo l'oggetto Card e l'aggiungo al dizionario

    def __str__(self):
        res = ''
        for cards_by_suit in self._cards.values():
            res += '-'.join([str(c) for c in cards_by_suit]) + '\n'
        return res

    def __len__(self):
        return sum(len(c) for c in self._cards.values())

    def __contains__(self, x):
        return x in self._cards[x.suit]

    def __iter__(self):
        for card_list in self._cards.values():
            for card in card_list:
                yield card

    def __getitem__(self, key):
        return self._cards[key]

    def pop(self):
        suits = list(self._cards.keys())
        random.shuffle(suits)
        for s in suits:
            if self._cards[s]:
                shuffled_cards = self._cards[s].copy()
                random.shuffle(shuffled_cards)
                card = shuffled_cards.pop()
                self._cards[s].remove(card)
                return card
            return None  # non ci sono più carte!


if __name__ == '__main__':

    deck = Deck()
    print('\nChiama il metodo __str__ di Deck')
    print(deck)
    print('\nChiama il metodo __len__ di Deck')
    print(len(deck))

    cuori_10 = NumberCard(10, Suits.heart)
    # __contains__
    print('\nChiama il metodo __contains__ di Deck')
    print('{} è nel mazzo? {}'.format(cuori_10, cuori_10 in deck))

    # __getitem__
    cuori = deck[Suits.heart]
    print('\nChiama il metodo __getitem__ di Deck per ottenere le carte di cuori',
          'e stampa la lista di oggetti Card (chiamata a __str__ di Card', sep='\n')
    print('-'.join(str(c) for c in cuori))

    #  __iter__
    print('\nChiama il metodo __iter__ di Deck')
    print('-'.join(str(c) for c in deck))

    c = deck.pop()
    while c:
        print('Carta estratta {}'.format(c))
        c = deck.pop()
