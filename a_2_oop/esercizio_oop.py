import datetime
import abc


class DiscountableItem(metaclass=abc.ABCMeta):

    def __init__(self, pub_date, title, price):
        self.pub_date = pub_date
        self.title = title
        self.price = price

    @abc.abstractmethod
    def discounted_price(self):
        raise NotImplementedError()


class Book(DiscountableItem):

    def discounted_price(self):
        return self.price if self.pub_date >= datetime.datetime(2016, 1, 1) else self.price - self.price * 30/100


class CD(DiscountableItem):

    def discounted_price(self):
        return self.price - self.price * 15/100


class Magazine(DiscountableItem):
    def discounted_price(self):
        pass


class Client:

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

    def discount_price(self, price):
        return price


class FidelityClient(Client):

    def discount_price(self, price):
        return price - price * 10/100


if __name__ == '__main__':
    book = Book(datetime.datetime(2014, 1, 1), 'Titolo libro', 15.40)
    client = FidelityClient('Mario', 'Rossi')

    new_price = client.discount_price(book.discounted_price())
    print('Il prezzo scontato è {:.2f} €'.format(new_price))

    client = Client('Biagio', 'Verdi')

    new_price = client.discount_price(book.discounted_price())
    print('Il prezzo scontato è {:.2f} €'.format(new_price))
    print(book)
    print(type(book))
    print(type(type(book)))
    print(type(type(type(book))))
