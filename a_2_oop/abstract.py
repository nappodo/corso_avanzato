import abc


class PluginBase(metaclass=abc.ABCMeta):

    @property
    @abc.abstractmethod
    def name(self):
        return '{}: {}'.format(str(type(self.__class__)), str(self.__class__.__name__))

    @abc.abstractmethod
    def load(self, input_):
        """Retrieve data from the input source and return an object."""
        return

    @abc.abstractmethod
    def save(self, output, data):
        """Save the data object to the output."""
        return


class SubclassImplementation(PluginBase):

    @property
    def name(self):
        return super().name

    def load(self, input_arg):
        return input_arg.read()

    def save(self, output, data):
        return output.write(data)

if __name__ == '__main__':
    print('Subclass:', issubclass(SubclassImplementation, PluginBase))
    print('Instance:', isinstance(SubclassImplementation(), PluginBase))
    a = SubclassImplementation()
    print(a.name)  # il tipo della classe non è più type ma abc.ABCMeta
    # b = PluginBase()  # TypeError: Can't instantiate abstract class PluginBase with abstract methods load, name, save
