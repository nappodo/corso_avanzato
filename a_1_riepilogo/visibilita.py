#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# # Variabile globale
# a = 0
#
# """
# Le variabili non contengono il valore ma il puntatore dell'oggetto.
# I parametri delle funzioni vengono passate per valore (by value).
# Quindi si ha un passaggio di puntatori by value (che equivale ad un passaggio per puntatore in C).
# """
#
#
# def func(c, d=100):
#     """
#     # c, d ed e sono variabili locali. All'uscita della funzione non saranno più visibili
#     # I valori di default, però, hanno visibilità nello scope immediatamente esterno,
#     # in questo caso è quello globale (cioè a livello di modulo)
#     """
#     e = 10
#     print(c, d, e)
#     # variable locale
#     a = 5
#     print(a)
#
#     def func2():
#         print(a)
#         def func3():
#             print(e)
#         func3()
#     func2()
#
# func(10)
# print(a)
#
#
# def func2(c, d=100):
#     global a
#     a += 999
#
# func2(10)
# print(a)
#
""" Attenzione ai valori di default mutabili!!! Ricordarsi che i valori di default hanno scope globale.
"""


def func3(x, l=[]):
    if not l:
        l.append(0)
    l.append(x)
    return l

l1 = func3(1000)
print(l1)
l2 = func3(5000)
print(l2)

# soluzione per evitare la condivisione globale di valori di default mutabili


def func4(x, l=None):
    if l is None:
        l = []
    l.append(x)


# Closure

def get_posts(site, ids):
    posts = site.posts.get(ids)

    def post_url(post):
        url = site.scheme + '://' + site.domain + '/' + post.slug + '.html'
        return url

    for p in posts:
        p.url = post_url(p)

    return posts

# Una closure più interessante
import re


def build_match_and_apply_functions(pattern, search, replace):
    def matches_rule(word):
        return re.search(pattern, word)

    def apply_rule(word):
        return re.sub(search, replace, word)
    return matches_rule, apply_rule
#
# rules = []
# with open('plural-rules.txt', encoding='utf-8') as pattern_file:
#     for line in pattern_file:
#         pattern, search, replace = line.split(None, 3)
#         rules.append(build_match_and_apply_functions(
#                 pattern, search, replace))
