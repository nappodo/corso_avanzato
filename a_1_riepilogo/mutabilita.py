#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""

"""
a = 1
b = a
a = 5
print(b)

input()
l = [1, 5, 8]
l1 = l
print(l1)
l[1] = 1000
print(l1)

input()

"""
Attenzione ai tipi immutabili che hanno riferimenti a tipi mutabili!!!
"""
t = ([1, 2], [1, 5])
# t[0] = 1000  # TypeError
print(t)
t[0][0] = 1000
print(t)
