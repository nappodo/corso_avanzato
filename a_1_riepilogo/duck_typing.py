#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Python è un linguaggio interpretato.
Il tipo delle variabili e degli argomenti, e dei valori di ritorno delle funzioni è definito a runtime.
L'interprete tenta di accedere all'attributo/metodo di un oggetto e se non esiste, o se l'operazione non è permessa
c'è un errore a runtime.
"""


def myfunc(a, b, c):
    """
    Se l'operazione + non è definita fra i tipi di a e b, ci sarà un'eccezione TypeError
    :param a: qualsiasi tipo per la quale è definita l'operazione + con il tipo di b
    :param b: qualsiasi tipo per la quale è definita l'operazione + con il tipo di a
    :param c: qualsiasi tipo per la quale è definita l'operazione * con il tipo di (a + b)
    """
    res = (a + b) * c
    return res


print(myfunc(1, 2, 3))
print(myfunc('az', 'bc', 3))
print(myfunc(['az'], ['bc'], 3))
print(myfunc(5, 4.5, 4))

input()
# print(myfunc([1, 'a'], 'a', 4))


class Duck:

    def quack(self):
        print('quack!')


class Person:

    def speak(self):
        print('Hallo!')

    def quack(self):
        print('I say "quack"!')


def quack(o):
    o.quack()


if __name__ == '__main__':
    duck = Duck()
    person = Person()
    quack(duck)
    quack(person)
