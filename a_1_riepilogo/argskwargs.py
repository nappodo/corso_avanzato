#!/usr/bin/env python
# -*- coding: UTF-8 -*-


def f(a, b, *numbers, **operations):
    res = a * b
    for i in numbers:
        res += i
    if operations.get('half'):
        res /= 2
    if operations.get('abs'):
        res = abs(res)
    return res

print(f(2, 2, 1, -5, -6, -7, 3, abs=True))
print(f(2, 2, 1, -5, -6, -7, 3, half=True, abs=True))

print(f(3, 3))

# dal database
nums = range(0, 500, 15)
multipliers = (3, 4)
kwargs = {'half': True}

print(f(*multipliers, *nums, **kwargs))


def g(a, b, c=0, d=1):
    pass

g(*(3,4), **dict(c=4, d=1))

g(3, 4, c=4, d=1)
# g(*args, **kwargs)