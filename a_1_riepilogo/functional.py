#!/usr/bin/env python
# -*- coding: UTF-8 -*-

print(' --------- FUNZIONI COME CITTADINI DI PRIMA CLASSE -----------')


def my_func(x, y):
    print(x, y)

# assegnazione di una funzione ad una variabile
a = my_func  # a contiene adesso il puntatore alla funzione my_func

print(type(a))  # <class 'function'>
a(12.5, 43.4)


# passaggio di funzione come argomento
def apply_func(f, x, y):
    f(x, y)

apply_func(a, 10, 1000)

# Iteratori
input()
print(' --------- ITERATORI -----------')
l = [1, 2, 3]
it = iter(l)
print(next(it))
print(next(it))

for i in it:
    print(i)  # resta soltanto un elemento da iterare

# tipi di dati che supportano l'iterazione (liste, tuple, set, dizionari (per chiave))
l = [100, 200, 300]
for i in l:
    print(i)
t = (0, 1, 3)
s = {'a', 'b', 'c'}


m = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6,
     'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}
for key in m:  # itera per chiave stesso che m.keys()
    print(key, m[key])

for v in m.values():  # itera sui valori
    print(v)

for k, v in m.items():  # itera sulle coppie (chiave, valore)
    print(v)

# anche i file supportano l'iterazione
f = open('../data/text.txt')
for line in f:
    print(line)
f.close()

input()
print(' --------- List|Set|Dictionary comprehension & Generator Expression  -----------')
l = range(100)
l1 = [i for i in l if i % 2 == 0]
l2 = []
for i in l:
    if i % 2 == 0:
        l2.append(i)
print(l2 == l1)
g = (i for i in l if i % 2 == 0)
s = {i for i in l if i % 2 == 0}
d = {i: chr(i + 65) for i in l}
print(l1)
print(s)
print(g)  # <generator object <genexpr> at 0x7fd163ee6150> Non è una sequenza ma piuttosto una funzione!
print(list(g))
print(d)
for i in g:
    print(i)

input()
# List comprehension un po' più complicata
l = [(i, [j for j in range(i) if j % 2 == 0]) for i in range(50) if i % 2 != 0]
print(l)

input()
print(' --------- GENERATORI  -----------')


def dispari(n):
    for i in range(n):
        if i % 2 != 0:
            yield i

for i in dispari(100):
    print(i)


input()
print(' --------- FUNZIONI PARZIALI  -----------')
from functools import partial


def log(message, subsystem):
    """Write the contents of 'message' to the specified subsystem."""
    print('%s: %s' % (subsystem, message))


server_log = partial(log, subsystem='server 192.168.122.15')
server_log('Unable to open socket')
server_log('Please retry later')

user_logged_fmt = '{user} process {p} - server: {server}'.format
user_john_server_1 = partial(user_logged_fmt, user='John', server='Server A')
processes = ['p1', 'p2', 'p3']
for p in processes:
    print(user_john_server_1(p=p))
