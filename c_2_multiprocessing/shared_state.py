import logging
import multiprocessing
from time import sleep

logging.basicConfig(level=logging.DEBUG, format='(%(processName)s) %(message)s')


def worker(d_, k, v):
    # sleep(10)
    logging.debug('Setting {}={}'.format(k, v))
    d_[k] = v

if __name__ == '__main__':
    mgr = multiprocessing.Manager()
    d = mgr.dict()
    jobs = [multiprocessing.Process(target=worker, args=(d, i, i * 2)) for i in range(10)]
    for j in jobs:
        j.start()
    for j in jobs:
        j.join()
    print(d)
