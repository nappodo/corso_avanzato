import multiprocessing
import time


class MyClass:
    def __init__(self, name):
        self.name = name

    def action(self):
        proc_name = multiprocessing.current_process().name
        print('Method action in {} from {}'.format(proc_name, self.name))


def worker(q):
    while True:
        obj = q.get()
        if obj is None:
            q.task_done()
            break
        obj.action()


class Consumer(multiprocessing.Process):
    def __init__(self, task_queue, result_queue):
        super().__init__()
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                self.task_queue.task_done()
                break
            print(self.name, next_task, sep=': ')
            result = next_task()
            self.task_queue.task_done()
            self.result_queue.put(result)


class Task:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, *args, **kwargs):
        time.sleep(0.01)
        return '{} * {}'.format(self.a, self.b)

    def __str__(self):
        return 'Task: {} * {}'.format(self.a, self.b)

if __name__ == '__main__':
    queue = multiprocessing.JoinableQueue()
    procs = [multiprocessing.Process(target=worker,
                                     args=(queue,)) for i in range(4)]

    for p in procs:
        p.start()

    _ = [queue.put(MyClass('obj-' + str(i))) for i in range(100)]
    for i in range(4):
        queue.put(None)  # poison pill - Flag to stop each worker
    # wait for the worker
    queue.close()
    queue.join_thread()
    for p in procs:
        p.join()

    input('\n\n\nAltro esempio di messaging')
    num_consumers = multiprocessing.cpu_count() * 2
    tasks = multiprocessing.JoinableQueue()  # ha il metodo task_done
    results = multiprocessing.Queue()
    consumers = [Consumer(tasks, results) for i in range(num_consumers)]
    for c in consumers:
        c.start()

    # jobs
    num_jobs = 50
    for i in range(num_jobs):
        tasks.put(Task(i, i))
    for i in range(num_consumers):
        tasks.put(None)
    tasks.join()  # chiamata bloccante. Aspetta che tutti i task abbiano lanciato task_done()

    while num_jobs:
        res = results.get()
        print('Result: ', res)
        num_jobs -= 1
