import timeit
import logging

from c_1_threading.utils import calculate_multiprocess, calculate_serial

logging.basicConfig(level=logging.DEBUG, format='(%(threadName)s) %(message)s')


if __name__ == "__main__":
    print('------------ > Number crunching')
    numbers = list(range(0, 1000))
    squaredNumbers = calculate_multiprocess(numbers, 6)
    squaredNumbers_serial = calculate_serial(numbers)
    t = timeit.Timer('calculate_multiprocess(list(range(0, 1000)), 4)', 'from c_1_threading.utils import calculate_multiprocess')
    print('Multiprocess: ', t.timeit(3))
    t1 = timeit.Timer('calculate_serial(list(range(0, 1000)))', 'from c_1_threading.utils import calculate_serial')
    print('Serial: ', t1.timeit(3))

    input('------------ > File processing')

    t = timeit.Timer('process_lines_multiprocess(lines, 4)',
                     'from c_1_threading.utils import square_number, process_lines_multiprocess\n'
                     'with open("../data/bigtext.txt") as f:\n'
                     '    lines = f.readlines()')
    print('Multiprocessing:', t.timeit(1))
    t1 = timeit.Timer('process_lines_serial(lines)',
                      'from c_1_threading.utils import square_number, process_lines_serial\n'
                      'with open("../data/bigtext.txt") as f:\n'
                      '    lines = f.readlines()')
    print('Serial:', t1.timeit(1))
    t = timeit.Timer('process_lines_multiprocess_2(lines, 4)',
                     'from c_1_threading.utils import square_number, process_lines_multiprocess_2\n'
                     'with open("../data/bigtext.txt") as f:\n'
                     '    lines = f.readlines()')
    print('Multiprocessing:', t.timeit(1))
