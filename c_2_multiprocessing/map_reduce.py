"""
Esempio di implementazione di Map Reduce multiprocessing.
Non è un vero e proprio Map Reduce perché non è distribuito.
"""

import collections
import itertools
import multiprocessing
import operator


class SimpleMapReduce:
    def __init__(self, map_func, reduce_func, num_workers=None):
        self.map_func = map_func
        self.reduce_func = reduce_func
        self.pool = multiprocessing.Pool(num_workers)

    @staticmethod
    def partition(mapped_values):
        data = collections.defaultdict(list)
        for k, v in mapped_values:
            data[k].append(v)
        return data.items()

    def __call__(self, inputs, chunksize=1):
        map_responses = self.pool.map(self.map_func, inputs, chunksize=chunksize)
        partitioned_data = self.partition(itertools.chain(*map_responses))
        reduced_values = self.pool.map(self.reduce_func, partitioned_data)
        return reduced_values

STOP_WORDS = {'anche', 'per', 'più', 'o', 'se', 'del', 'dal', 'dalla', 'della', 'al', 'cui', 'non', 'che', 'delle',
              'tra', 'fra', 'e', 'a', 'la', 'lo', 'le', 'su', 'sui', 'come', 'si', 'sin', 'ha', 'con', 'coi',
              'in', 'il', 'un', 'una', 'è', 'da', 'i', 'di', 'del', 'dei', 'nel', 'nella', 'negli', 'nei', 'nello', 'ad'}


# Map function
def process_file(filename):
    print(multiprocessing.current_process().name, 'reading', filename)
    output = []
    with open(filename) as f:
        for line in f:
            for word_ in line.split():
                word_ = word_.strip('.,')
                if word_.lower() not in STOP_WORDS:
                    output.append((word_, 1))

    return output


# Reduce functions
def count_words(item):
    word_, occurrences = item
    return word_, sum(occurrences)


if __name__ == '__main__':
    input_files = ['../data/bigtext.txt',
                   '../data/text.txt',
                   '../data/bigtext.txt',
                   '../data/text.txt']
    mapred = SimpleMapReduce(process_file, count_words)
    word_counts = mapred(input_files, 2)
    word_counts.sort(key=operator.itemgetter(1), reverse=True)
    for word, counter in word_counts:
        print(word, counter)
