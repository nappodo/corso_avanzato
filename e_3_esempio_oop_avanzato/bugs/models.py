from .db import DBA

DB_FILE = '../data/orm_homegrown.db'


class DBActiveObject:
    fields = ()
    table = ''
    pk_field = ''
    fks = {}
    stmt_get_template = 'SELECT {} FROM {} WHERE {}=?'
    stmt_insert_template = 'INSERT INTO {}({}) VALUES({}) ON DUPLICATE KEY UPDATE {}'
    stmt_delete_template = 'DELETE FROM {} WHERE {}=?'
    stmt_update_template = 'UPDATE {} SET {} WHERE {}=?'

    def __init__(self, *args):
        if len(args) != len(self.fields):
            raise TypeError('Wrong # args')
        for name, val in zip(self.fields, args):
            setattr(self, name, val)

    def __str__(self):
        return ',\n'.join('{}: {}'.format(col, getattr(self, col)) for col in self.fields)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    @classmethod
    def get(cls, pk):
        select_fields = ', '.join([f for f in cls.fields])
        stmt = cls.stmt_get_template.format(select_fields, cls.table, cls.pk_field)
        with DBA(DB_FILE) as db:
            db(stmt, (str(pk),))
            row = db.result  # property only for single row result set
            account = cls.build(row) if row else None
        return account

    def save(self):
        insert_stmt_dict = self.stmt_insert_template.format(
            self.table,
            ','.join(self.fields),
            ','.join(['%s']*len(self.fields)),
            ','.join(['{}=VALUES({})'.format(col, col) for col in self.fields])
        )
        args = [getattr(self, field) for field in self.fields]

        with DBA(DB_FILE) as db:
            db(insert_stmt_dict, tuple(args))

    def delete(self):
        stmt = self.stmt_delete_template.format(self.table, self.pk_field)
        pk = getattr(self, self.pk_field)
        with DBA(DB_FILE) as db:
            db(stmt, (str(pk),))

    @classmethod
    def build(cls, row):
        args = []
        for k in cls.fields:
            if k in cls.fks:
                class_fk = cls.fks[k]
                object_fk = class_fk.get(pk=row[k])
                args.append(object_fk)
            else:
                args.append(row[k])
        return cls(*args)


class Account(DBActiveObject):
    table = 'accounts'
    fields = ('account_id', 'account_name')
    pk_field = 'account_id'


class Product(DBActiveObject):
    table = 'products'
    fields = ('product_id', 'product_name')
    pk_field = 'product_id'


class Bug(DBActiveObject):
    table = 'bugs'
    fields = ('bug_id', 'bug_description', 'bug_status', 'reported_by', 'assigned_to', 'verified_by')
    pk_field = 'bug_id'
    fks = {'reported_by': Account,
           'assigned_to': Account,
           'verified_by': Account}


