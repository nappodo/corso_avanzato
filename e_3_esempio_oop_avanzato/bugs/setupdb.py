from mysql.connector import Error
from bugs.db import init_db, populate_db
import sys
if __name__ == '__main__':
    print(sys.argv)
    populate = False
    if len(sys.argv) > 1:
        options = sys.argv[1:]
        populate = options[0] == 'populate'
    try:
        init_db()
        if populate:
            populate_db()
    except Error as e:
        print('ERROR - Cannot init DB: ', e)
