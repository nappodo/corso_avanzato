CREATE TABLE accounts (
  account_id        INTEGER NOT NULL PRIMARY KEY,
  account_name      VARCHAR(100) NOT NULL
);

CREATE TABLE products (
  product_id        INTEGER NOT NULL PRIMARY KEY,
  product_name      VARCHAR(100)
);

CREATE TABLE bugs (
  bug_id            INTEGER NOT NULL PRIMARY KEY,
  bug_description   VARCHAR(100),
  bug_status        VARCHAR(20),
  reported_by       INTEGER REFERENCES accounts(account_name),
  assigned_to       INTEGER REFERENCES accounts(account_name),
  verified_by       INTEGER REFERENCES accounts(account_name)
);

CREATE TABLE bugs_products (
  bug_id            INTEGER NOT NULL REFERENCES bugs,
  product_id        INTEGER NOT NULL REFERENCES products,
  PRIMARY KEY       (bug_id, product_id)
);