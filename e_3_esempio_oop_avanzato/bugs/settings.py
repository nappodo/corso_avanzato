DB_CONF = {'user': 'synergia',
           'password': 'synergia',
           'host': 'localhost',
           'database': 'synergia',
           'port': 3306}

DB_POOL = {'pool_name': 'pool_synergia',
           'pool_size': 5,
           }

DB_POOLED_CONF = DB_CONF.copy()
DB_POOLED_CONF.update(DB_POOL)
