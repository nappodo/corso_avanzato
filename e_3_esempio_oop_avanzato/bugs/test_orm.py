from .models import Account, Bug

if __name__ == '__main__':

    a = Account(9, 'Valerio')
    a.save()  # inserisce riga in tabella
    b = Account.get(pk=9)
    assert b.account_name == 'Valerio'
    assert a == b  # True (implementare il metodo __eq__)
    b.delete()

    c = Account.get(pk=9)
    assert c is None

    # #####################
    #
    existing_account = Account(3, 'Fabio')
    existing_account.save()  # dovrebbe fare l'UPDATE della riga 3 perché esiste già
    #######################


    #######################
    bug = Bug.get(pk=2)
    assert bug.reported_by == Account.get(1)
    #
    # #####################
    #
    # bugs_project_2 = Bug.filter(assigned_to=2)
    # assert Bug.get(1) in bugs_project_2
    # assert Bug.get(3) in bugs_project_2
