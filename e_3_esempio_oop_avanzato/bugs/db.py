import json
import sqlite3
from .settings import DB_CONF


class DBA:
    """
    Classe di accesso al database che implementa il protocollo context manager
    Utilizzo:
    with DBA(db_file) as dba:
        dba('SHOW DATABASES')
        for (res,) in dba:
            print(res)
    """
    def __init__(self, db_file, prepared=False):

        self._cnx = sqlite3.connect(db_file)
        self._cur = self._cnx.cursor(dictionary=True)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        if isinstance(exc_val, Exception):
            print('Rollback!')
            self._cnx.rollback()
        else:
            self._cnx.commit()
        self._cur.close()
        self._cnx.close()

    def __call__(self, *args, **kwargs):
        """
        Fa dell'oggetto DBA un callable:
        dba = DBA(configuration)
        dba('SELECT * from acconts')
        :param args: contiene tupla di parametri così come passati al cursore mysql
        :param parametri in kwargs:
            multiple_ddl = True se l'SQL consiste in istruzioni DDL multiple separate da ';'
        :return:
        """
        # print(args)
        #
        # if kwargs.get('multiple_ddl'):
        #     res = self._cur.execute(*args, multi=True)
        #     for r in res:
        #         if r.with_rows:
        #             print(r.fetchall())
        #         else:
        #             print("# of rows affected by statement '{}': {}".format(r.statement, r.rowcount))
        #     return res
        self._cur.execute(*args)

    def __iter__(self):
        return iter(self._cur)

    @property
    def result(self):
        # property only for single row result set
        return self._cur.fetchone()


def init_db():
    with DBA(DB_CONF) as db:
        delete_sql = open('fixtures/delete_tables.sql').read()
        create_sql = open('fixtures/create_tables.sql').read()
        db(delete_sql, multiple_ddl=True)
        db(create_sql, multiple_ddl=True)

    with DBA(DB_CONF) as db:
        db('SHOW TABLES')
        for res in db:
            print(res)


def populate_db():

    prep_insert_stmt_dict = {}
    stmt_tmpl = 'INSERT INTO {}({}) VALUES ({})'

    with DBA(DB_CONF, prepared=True) as db:
        data = json.loads(open('fixtures/populate.json').read())
        for table in data:
            print('Populating: ', table)

            rows = data[table]
            for row in rows:
                vals = row.values()
                if not prep_insert_stmt_dict.get(table):
                    cols = row.keys()
                    prep_insert_stmt_dict[table] = stmt_tmpl.format(table, ','.join(cols), ','.join(['%s']*len(cols)))
                # print(prep_insert_stmt_dict[table], vals)
                # chiamata al DB
                db(prep_insert_stmt_dict[table], tuple(vals))
