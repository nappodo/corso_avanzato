from router.ctools import NetworkElement, RouteInspector
from router.ctools.exception import MissingVar

# make it PEP-8 compliant

ne = NetworkElement('171.0.1.125')
try:
    routing_table = ne.getRoutingTable()
except MissingVar:
    ne.cleanup('rollback')
else:
    num_routes = routingTable.getSize()
    for rt_offset in range(num_routes):
        route = routing_table.getRouteByIndex(rt_offset)
        name = route.getName()
        ip = route.getIpAddr()
        print('Checking %s %s' % (name, ip))
        RouteInspector.inspect(route)
finally:
    ne.cleanup('commit')
    ne.disconnect()



# ========================================================
from myroutertools import NetworkElement, RouteInspector
with NetworkElement('171.0.1.125') as ne:
    for route in ne:
        print('Checking %s %s' % (route.name, route.ip))
        RouteInspector.inspect(route)


class NetworkElement(router.ctools.NetworkElement):
    @property
    def routing_table(self):
        return self.getRoutingTable()

    def __iter__(self):
        rout_tab = self.routing_table
        size = rout_tab.getSize()
        for i in range(size):
            yield rout_tab.getRouteByIndex(i)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if isinstance(exc_type, MissingVar):
            self.cleanup('rollback')
        else:
            ne.cleanup('commit')
        self.disconnect()
