from setuptools import setup, find_packages

packages = find_packages()
print(packages)

setup(
    name='Flask esempio web',
    version='1.0',
    packages=['d_2_web.rubrica_orm'],
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'SQLAlchemy'],
    entry_points={'console_scripts': ['launch_rubrica_web = d_2_web.rubrica_orm.script:main']},
)


"""
Per creare eseguibili windows, si può utilizzare py2exe,
che impacchetta tutte le librerie windows e python che servono

Basta importare py2exe all'interno del file setup.py

import py2exe
setup(
    name='Flask esempio web',
    version='1.0',
    packages=['d_2_web.rubrica_orm'],
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'SQLAlchemy']
)

"""