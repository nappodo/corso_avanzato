class InvalidWithdrawal(Exception):

    def __init__(self, balance, amount):
        super().__init__("Account doesn't have ${}".format(amount))
        self.amount = amount
        self.balance = balance

    def overage(self):
        return self.amount - self.balance


class ExceededInvalidWithdrawal(InvalidWithdrawal):
    pass


class QualcosaInvaliWithdrawal(InvalidWithdrawal):
    pass


class Account:
    def __init__(self, balance):
        self.balance = balance

    def withdraw(self, amount):
        """

        :param amount: int, amount to withdraw
        :raises InvalidWithdraw exception
        :return:
        """
        if amount > self.balance:
            raise ExceededInvalidWithdrawal(self.balance, amount)
        self.balance -= amount


if __name__ == '__main__':
    account = Account(125)
    try:
        account.withdraw(100)
        account.withdraw(50)
    except QualcosaInvaliWithdrawal as e:
        pass
    except InvalidWithdrawal as e:
        print("Sorry but you exceeded your balance for ${}".format(e.overage()))
        print("Exception {}".format(type(e)))
    # account.withdraw(100)
