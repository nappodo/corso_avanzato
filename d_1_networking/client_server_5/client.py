import asyncore
import logging
import socket
from io import StringIO
from urllib import parse


# Nota: asyncore sarà deprecata in futuro e poi rimossa
class HttpClient(asyncore.dispatcher):
    def __init__(self, url):
        self.url = url
        self.logger = logging.getLogger(self.url)

        asyncore.dispatcher.__init__(self)
        self.write_buffer = 'GET {} HTTP/1.0\r\n\r\n'.format(self.url)
        self.read_buffer = StringIO()
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)  # metodo di asyncore.dispatcher

        address = (parse.urlparse(self.url).netloc, 80)
        self.logger.debug('connecting to %s', address)
        self.connect(address)

    def handle_connect(self):
        self.logger.debug('handle_connect()')

    def handle_close(self):
        self.logger.debug('handle_close()')
        self.close()

    def writable(self):
        is_writable = (len(self.write_buffer) > 0)
        if is_writable:
            self.logger.debug('writable() -> %s', is_writable)
        return is_writable

    def readable(self):
        self.logger.debug('readable() -> True')
        return True

    def handle_write(self):
        sent = self.send(bytes(self.write_buffer, encoding='ascii'))
        self.logger.debug('handle_write() -> "%s"', self.write_buffer[:sent])
        self.write_buffer = self.write_buffer[sent:]

    def handle_read(self):
        data = self.recv(8192)
        self.logger.debug('handle_read() -> %d bytes', len(data))
        self.read_buffer.write(str(data))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(name)s: %(message)s')
    WEBSITES = ['http://example.com/', 'https://pymotw.com/2/asynchat/', 'http://example.net/']
    clients = [HttpClient(s) for s in WEBSITES]

    logging.debug('LOOP STARTING')

    # il loop chiama handle_read e handle_write sulla base dei valori ritornati da is_readable e is_writable
    asyncore.loop()

    logging.debug('LOOP DONE')

    for c in clients:
        response_body = c.read_buffer.getvalue()
        print('\n')
        print(c.url, 'got', len(response_body), 'bytes')