import socket
import struct

# Crea il TCP/IP socket
# si connette il socket al server

port = 10001
server_address = ('localhost', port)
print('Connect to server on port [{}]'.format(port))
sock = socket.create_connection(server_address)
packer = struct.Struct('I 2s f')

try:
    message = ''
    while True:
        message = input('Messaggio da inviare al server....')
        if message == 'QUIT':
            break
        splits = message.split()
        i, s, f = int(splits[0]), bytes(splits[1], encoding='UTF-8'), float(splits[2])
        packed = packer.pack(i, s, f)
        sock.sendall(packed)
finally:
    sock.close()
