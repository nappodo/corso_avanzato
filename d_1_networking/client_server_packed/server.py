import signal
import socket

import struct
import sys

# Crea il TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# avvia il server sulla porta 10001
port = 10001
server_address = ('localhost', port)
print('Start server on port [{}]'.format(port))
sock.bind(server_address)
unpacker = struct.Struct('I 2s f')
# Listen
sock.listen(1)


def signal_handler(signal, frame):  # intercetta CTRL+C tramite signal.SIGINT
    sock.close()
    print('Server shut down...')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

while True:

    try:
        # wait for connection
        connection, client_address = sock.accept()
        print(u'Connection received from ', client_address)
        while True:
            data = connection.recv(4096)
            try:
                i, s, f = unpacker.unpack(data)
            except struct.error as e:
                print('ERROR;', data)
                print(e)
                break
            if data:
                print('Data received from client', (i, s, f))
            else:
                print('No more data from client:', client_address)
                break
    except Exception as e:
        print(e)
    finally:
        print('Closing connection with', client_address)
        connection.close()

