import socket


# UDP è message oriented invece che stream oriented (utile per messaggi di piccole dimensioni)
# si connette il socket al server
port = 10000
server_address = ('localhost', port)
print('Connect to server on port [{}]'.format(port))
# Crea socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    try:
        message = ''
        while message != 'QUIT':
            message = input('Messaggio da inviare al server....')
            sock.sendto(bytes(message, encoding='UTF-8'), server_address)
            data, from_address = sock.recvfrom(4096)
            print('Received {}'.format(data))
    finally:
        sock.close()
