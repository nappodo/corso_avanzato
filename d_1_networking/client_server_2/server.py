import signal
import socket

import sys
# UDP è message oriented invece che stream oriented (utile per messaggi di piccole dimensioni)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# avvia il server sulla porta 1000
port = 10000
server_address = ('localhost', port)
print('Start server on port [{}]'.format(port))
sock.bind(server_address)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


def signal_handler(signal, frame):  # intercetta CTRL+C tramite signal.SIGINT
    sock.close()
    print('Server shut down...')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

while True:
    # wait for connection
    data, client_address = sock.recvfrom(4096)
    print('Data received from {}'.format(client_address))
    print(data)
    if data:
        print(r'Data received from client: {}'.format(data))
        sent = sock.sendto(data, client_address)
