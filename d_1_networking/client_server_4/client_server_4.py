import logging
import socketserver

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )

# Quasi tutti i metodo sono sovrascritti per esigenze di logging.


class EchoRequestHandler(socketserver.BaseRequestHandler):
    def __init__(self, request, client_address, server_):
        self.logger = logging.getLogger('EchoRequestHandler')
        self.logger.debug('__init__')
        socketserver.BaseRequestHandler.__init__(self, request, client_address, server_)
        return

    # nella maggior parte dei casi ci serve soltanto sovrascrivere questo metodo
    def handle(self):
        self.logger.debug('handle')
        # Echo the back to the client
        data = self.request.recv(1024)
        self.logger.debug('recv()->"%s" from %s', data, self.client_address)
        self.request.send(data)
        return


if __name__ == '__main__':
    import socket
    import threading

    address = ('localhost', 0)  # let the kernel give us a port
    server = socketserver.TCPServer(address, EchoRequestHandler)  # __init__ ha di default EchoRequestHandler come handler_class
    ip, port = server.server_address  # find out what port we were given

    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True)
    t.start()

    logger = logging.getLogger('client')
    logger.info('Server on %s:%s', ip, port)

    # Client part
    # Connect to the server
    logger.debug('creating socket')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    logger.debug('connecting to server')
    s.connect((ip, port))
    # second client
    s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s1.connect((ip, port))
    # Send the data
    message = b'Hello, world'
    logger.debug('sending data: "%s"', message)
    len_sent = s.send(message)
    len_sent = s1.send(message)

    # Receive a response
    logger.debug('waiting for response')
    response = s.recv(len_sent)
    response1 = s1.recv(len_sent)
    logger.debug('response from server: "%s"', response)

    # Clean up
    logger.debug('closing socket')
    s.close()
    s1.close()
    logger.debug('done')
    server.socket.close()
