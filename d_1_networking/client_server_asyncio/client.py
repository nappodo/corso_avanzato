import asyncio


class EchoClientProtocol(asyncio.Protocol):
    def __init__(self, message, loop):
        self.message = message
        self.loop = loop

    def connection_made(self, transport):
        transport.write(self.message.encode())
        print('Data sent: {!r}'.format(self.message))

    def data_received(self, data):
        print('Data received: {!r}'.format(data.decode()))

    def connection_lost(self, exc):
        print('The server closed the connection')
        print('Stop the event loop')
        self.loop.stop()

loop_ = asyncio.get_event_loop()
message_ = 'Hello World!'
coro = loop_.create_connection(lambda: EchoClientProtocol(message_, loop_), '127.0.0.1', 10888)
loop_.run_until_complete(coro)
loop_.run_forever()
loop_.close()   # arriva qui dopo self.loop.stop()
