import abc
from asyncio import *
from aioconsole import ainput

MENU = '''
---------------------------
l: Lista contatti
a: aggiungi contatto
m: modifica contatto
c: cancella contatto
w: mostra menu
x: Esci
---------------------------
'''

loop_ = get_event_loop()


class Command(metaclass=abc.ABCMeta):
    asyn = False

    def __init__(self, tcp_client):
        self.client = tcp_client

    @abc.abstractmethod
    def run(self):
        raise NotImplementedError()


class ListCommand(Command):
    def run(self):
        self.client.send_data_to_tcp('LIST:')


class AddCommand(Command):
    asyn = True
    async def run(self):
        s = await ainput('Inserisci contatto (es. Nome:06555555) ###  ')
        self.client.send_data_to_tcp('ADD:' + s)


class QuitCommand(Command):
    def run(self):
        self.client.send_data_to_tcp('DISCONNECT:')
        print('Buona giornata!!!')
        loop_.stop()
        loop_.close()
        exit()


class RemoveCommand(Command):
    asyn = True
    async def run(self):
        s = await ainput('Inserisci nome da eliminare ###  ')
        self.client.send_data_to_tcp('REMOVE:' + s)


class UpdateCommand(Command):
    asyn = True
    async def run(self):
        s = await ainput('Aggiorna contatto (es. Nome:06555555) ###  ')
        self.client.send_data_to_tcp('UPDATE:' + s)


class CommandFactory:
    _cmds = {'l': ListCommand,
             'a': AddCommand,
             'm': UpdateCommand,
             'c': RemoveCommand,
             'x': QuitCommand}

    @classmethod
    def get_cmd(cls, cmd):
        cmd = cmd.strip()
        cmd_cls = cls._cmds.get(cmd)
        return cmd_cls


class RubricaClient(Protocol):
    def __init__(self, loop):
        self.loop = loop
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        print('Data received from server: \n===========\n{}\n===========\n'.format(data.decode()), flush=True)

    def send_data_to_tcp(self, data):
        self.transport.write(data.encode())

    def connection_lost(self, exc):
        print('The server closed the connection')
        print('Stop the event loop')
        self.loop.stop()


def menu():
    print(MENU)


async def main():
    menu()
    while True:
        cmd = await ainput('Rubrica >')
        cmd_cls = CommandFactory.get_cmd(cmd)
        if not cmd_cls:
            print('Comando non riconosciuto: {}'.format(cmd))
        elif cmd_cls.asyn:
            await cmd_cls(client).run()
        else:
            cmd_cls(client).run()


if __name__ == '__main__':
    client = RubricaClient(loop_)
    coro = loop_.create_connection(lambda: client, '127.0.0.1', 10888)
    loop_.run_until_complete(coro)
    loop_.run_until_complete(main())
