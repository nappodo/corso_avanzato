import sqlite3


class RubricaDB:
    def __init__(self, dbfile):
        self.cnx = sqlite3.connect(dbfile)
        self.cur = None
        self._init_db()  # create table if not exists...

    def _init_db(self):
        sql_create_contatti = """CREATE TABLE IF NOT EXISTS rubrica(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              name text,
              number text
            )
        """
        self.cur = self.cnx.cursor()
        self.cur.execute(sql_create_contatti)
        self.cnx.commit()
        self.cur.close()

    def close(self):
        if self.cur:
            self.cur.close()
        self.cnx.close()

    def __enter__(self):
        self.cur = self.cnx.cursor()
        return self

    def __call__(self, *args, **kwargs):
        if not self.cur:
            self.cur = self.cnx.cursor()
        self.cur.execute(*args, **kwargs)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if isinstance(exc_val, Exception):
            self.cnx.rollback()
        else:
            self.cnx.commit()
        self.cur.close()

    def __iter__(self):
        return iter(self.cur)

    def list(self):
        stmt = "SELECT name, number FROM rubrica"
        with self:
            self(stmt)
            res = {i[0]: i[1] for i in self}
        return res

    def remove(self, name):
        stmt = "DELETE FROM rubrica WHERE name=?"
        with self:
            self(stmt, (name,))

    def add(self, name, number):
        stmt = "INSERT INTO rubrica(name, number) VALUES(?, ?)"
        with self:
            self(stmt, (name, number))

    def update(self, nome, numero):
        stmt = "UPDATE rubrica SET number=? WHERE name=?"
        with self:
            self(stmt, (nome, numero))
