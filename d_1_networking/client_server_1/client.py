import socket


# Crea il TCP/IP socket
# si connette il socket al server
port = 10000
server_address = ('localhost', port)
print('Connect to server on port [{}]'.format(port))
connection = socket.create_connection(server_address)

try:
    message = ''
    while message != 'QUIT':
        message = input('Messaggio da inviare al server....')
        connection.sendall(bytes(message, encoding='UTF-8'))
        recv_size = 0
        while recv_size < len(message):
            data = connection.recv(64)
            recv_size += len(data)
            print('Received {} from server'.format(data))
finally:
    connection.close()
