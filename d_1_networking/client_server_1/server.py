import signal
import socket
import sys


def signal_handler(signal, frame):  # intercetta CTRL+C tramite signal.SIGINT
    sock.close()
    print('Server shut down...')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)


# Crea il TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# avvia il server sulla porta 1000
port = 10000
server_address = ('localhost', port)
print('Start server on port [{}]'.format(port))
sock.bind(server_address)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Listen
sock.listen()


while True:
    # wait for connection
    connection, client_address = sock.accept()  # blocking I/O
    print('Connection received from {}'.format(client_address))
    try:
        while True:
            data = connection.recv(64)  # blocking I/O
            if data:
                print(r'Data received from client: {}'.format(data))
                connection.sendall(data)
            else:
                print(r'No more data from client: {}'.format(client_address))
                break
    finally:
        connection.close()
