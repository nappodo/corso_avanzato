import abc
import sqlite3


class RubricaDB:
    def __init__(self, dbfile):
        self.cnx = sqlite3.connect(dbfile)
        self.cur = None
        self._init_db()  # create table if not exists...

    def _init_db(self):
        sql_create_contatti = """CREATE TABLE IF NOT EXISTS rubrica(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              name text,
              number text
            )
            """
        self.cur = self.cnx.cursor()
        self.cur.execute(sql_create_contatti)
        self.cnx.commit()
        self.cur.close()

    def close(self):
        if self.cur:
            self.cur.close()
        self.cnx.close()

    def __enter__(self):
        self.cur = self.cnx.cursor()
        return self

    def __call__(self, *args, **kwargs):
        if not self.cur:
            self.cur = self.cnx.cursor()
        self.cur.execute(*args, **kwargs)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if isinstance(exc_val, Exception):
            self.cnx.rollback()
        else:
            self.cnx.commit()
        self.cur.close()

    def __iter__(self):
        return iter(self.cur)

    def list(self):
        stmt = "SELECT name, number FROM rubrica"
        with self:
            self(stmt)
            res = {i[0]: i[1] for i in self}
        return res

    def remove(self, name):
        stmt = "DELETE FROM rubrica WHERE name=?"
        with self:
            self(stmt, (name,))

    def add(self, name, number):
        stmt = "INSERT INTO rubrica(name, number) VALUES(?, ?)"
        with self:
            self(stmt, (name, number))

    def update(self, nome, numero):
        stmt = "UPDATE rubrica SET number=? WHERE name=?"
        with self:
            self(stmt, (nome, numero))


class Command(metaclass=abc.ABCMeta):
    @abc.abstractclassmethod
    def run(self, phonebook):
        raise NotImplementedError()


class ListCommand(Command):
    @classmethod
    def run(cls, rubrica_db):
        res = rubrica_db.list()
        if not res:
            print('>>>>>>>>>>> Rubrica Vuota <<<<<<<<<<<<<')
            return
        for name, phone in res.items():
            print('{}: {}'.format(name, phone))


class AddCommand(Command):
    @classmethod
    def run(cls, rubrica_db):
        s = input('Inserisci contatto (es. Nome:06555555) ###  ')
        name, phone = s.split(':')
        rubrica_db.add(name, phone)
        print('Contatto {} inserito'.format(name))


class QuitCommand(Command):
    @classmethod
    def run(cls, rubrica_db):
        rubrica_db.close()
        print('Buona giornata!!!')
        exit()


class RemoveCommand(Command):
    @classmethod
    def run(cls, rubrica_db):
        s = input('Inserisci nome da eliminare ###  ')
        rubrica_db.remove(s)
        print('Contatto {} eliminato'.format(s))


class UpdateCommand(Command):
    @classmethod
    def run(cls, rubrica_db):
        s = input('Aggiorna contatto (es. Nome:06555555) ###  ')
        name, phone = s.split(':')
        rubrica_db.update(name, phone)
        print('Contatto {} aggiornato'.format(name))


class ShowMenuCommand(Command):
    @classmethod
    def run(cls, phonebook):
        menu()


class CommandFactory:
    _cmds = {'l': ListCommand,
             'a': AddCommand,
             'm': UpdateCommand,
             'c': RemoveCommand,
             'w': ShowMenuCommand,
             'x': QuitCommand}

    @classmethod
    def get_cmd(cls, cmd):
        cmd_cls = cls._cmds.get(cmd)
        return cmd_cls


FILENAME = '../data/rubrica.db'

MENU = '''
---------------------------
l: Lista contatti
a: aggiungi contatto
m: modifica contatto
c: cancella contatto
w: mostra menu
x: Esci
---------------------------
'''


def menu():
    print(MENU)


def main():
    rubrica_db = RubricaDB(FILENAME)
    menu()
    while True:
        command = input('Rubrica Telefonica >  ')
        cmd_cls = CommandFactory.get_cmd(command)
        if not cmd_cls:
            print('Comando non riconosciuto: {}'.format(command))
            continue
        cmd_cls.run(rubrica_db)

if __name__ == '__main__':
    main()
