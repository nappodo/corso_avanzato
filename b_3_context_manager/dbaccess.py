import sqlite3


class DBA:
    """
    Classe di accesso al database che implementa il protocollo context manager

    with DBA(config) as dba:
        dba('SHOW DATABASES')
        for (res,) in dba:
            print(res)
    """
    def __init__(self, file):
        # Attenzione: la classe è strettamente "accoppiata" con la particolare implementazione DB
        # che stiamo utilizzando (sqlite3).
        # Un design ottimale prevede che una classe di questo tipo sia disaccoppiata dalle implementazioni dello storage
        self._cnx = sqlite3.connect(file)
        self._cur = self._cnx.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._cur.close()
        if isinstance(exc_val, Exception):
            self._cnx.rollback()
        else:
            self._cnx.commit()
        self._cnx.close()

    def __call__(self, *args, **kwargs):
        self._cur.execute(*args, **kwargs)

    def __iter__(self):
        for item in self._cur:
            yield item

    def init_test_data(self):
        # Create table
        self._cur.execute('''DROP TABLE IF EXISTS stocks''')
        self._cur.execute('''CREATE TABLE stocks
(date text, trans text, symbol text, qty real, price real)''')

        # Insert a row of data
        self._cur.execute("INSERT INTO stocks "
                          "VALUES ('2006-01-05','BUY','RHAT',100,35.14)")
        # Larger example that inserts many records at a time
        purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
                     ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
                     ('2006-04-06', 'SELL', 'RHAT', 500, 53.00),
                     ]
        self._cur.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)',
                              purchases)


if __name__ == '__main__':

    with DBA('../data/example.db') as dba:
        # dba.init_test_data()
        symbol = 'RHAT'
        dba('SELECT * FROM stocks WHERE symbol = ?', (symbol,))
        for res in dba:
            print(res)



