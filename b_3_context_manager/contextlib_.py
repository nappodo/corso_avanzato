from contextlib import contextmanager


@contextmanager
def make_context():
    print('---> Entering in context')
    try:
        yield 'Context object'
    except RuntimeError as e:
        print('Error')
    finally:
        print('<---- Exiting context')


print('OUT of context')
with make_context() as value:
    print(value)
print('OUT of context')