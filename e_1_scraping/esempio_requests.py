import itertools
import requests
from bs4 import BeautifulSoup

pages = ['http://www.seeker.com/', 'http://www.thedodo.com']
for i, p in enumerate(pages):
    res = requests.get(p)
    if res and res.status_code == 200:
        with open('filename_' + str(i) + '.html', 'wt', encoding='UTF-8') as f:
            soup = BeautifulSoup(res.text, 'html5lib')
            for script in itertools.chain(soup.find_all('script'),
                                          soup.find_all('noscript'),
                                          soup.find_all('style')):
                script.extract()  # rimuove dal soup originale
            f.write(soup.prettify())

            # f.write(res.text)
