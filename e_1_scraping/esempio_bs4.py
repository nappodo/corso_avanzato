post = """
<div>
teststs
<h1>Titolo</h1><h2>Sottotitolo</h2>
    <p>Testo</p>
    <a href="/images"><img src="/images/img.png" /></a>
    <a href="/"><img src="/images/img.png" /></a>
    <a class="out" href="http://www.sito1.com"><img src="/images/img.png" /></a>
    <p class="small">Altro testo</p>
</div>
"""
from bs4 import BeautifulSoup

soup = BeautifulSoup(post, 'html5lib')  # html.parser default, lxml.xml
print(soup)
print(soup.prettify())
input()
print(soup.h1)
print(soup.a.img)
input()
res = soup.find_all('a')
for a in res:
    print(a['href'])

input()
res = soup.find_all('a', attrs={'class': 'out'})
print('\n\n')

for a in res:
    print(a['href'])
input()
print(soup.p.text)
res = soup.find_all('p')
for p in res:
    print('testo di ', p, p.text)

input()
print(soup(['h1', 'h2']))
for p in soup.find_all('p'):
    p.extract()
print(soup.prettify())