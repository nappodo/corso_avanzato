import requests
from bs4 import BeautifulSoup

# scrap from
# http://www.brainyquote.com/quotes/topics/topic_life.html
# http://www.brainyquote.com/quotes/topics/topic_life2.html
# http://www.brainyquote.com/quotes/topics/topic_life3.html
# ...

BASE_URL = 'http://www.brainyquote.com/quotes/topics/topic_life{}.html'


def quote_to_line(q):
    return '{} ({}) - {}\n'.format(q['text'], q['author'], q['link'])


def get_quotes(page):
    result = []
    res = requests.get(page)
    if res.status_code == 200:
        content = res.text
        soup = BeautifulSoup(content, 'html.parser')
        elements = soup.find_all('div', attrs={'class': 'boxyPaddingBig'})
        for div in elements:
            quote = dict(link=div.span.a['href'],
                         text=div.span.a.text,
                         author=div.div.a.text)
            result.append(quote)
    return result


def generate_urls():
    yield BASE_URL.format('')
    for i in range(2, 10):
        yield BASE_URL.format(i)


if __name__ == '__main__':
    with open('quotes.txt', 'w') as f:
        for page in generate_urls():
            quotes = get_quotes(page)
            for q_ in quotes:
                f.write(quote_to_line(q_))
